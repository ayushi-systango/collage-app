import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Home.css';
import GetStarted from './GetStarted';
import history from '../../history';

const Home = (props)=> {
  return (
    <div className="container">
    <div className="logo">
    <img src={require('../../assests/logo/logo.png')}/>
    </div>
     <img src={require('../../assests/logo/Background.png')}
      alt="network error"
       width="500"></img>
      <button onClick={()=>history.push('/start')} 
      className="btn1">
      Get Started
      </button>
      <button onClick={()=>history.push('/signup')}
      className="btn2" >Login</button>
    </div>
  );
}

export default Home;
