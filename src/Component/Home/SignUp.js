import React from 'react';
import {Link} from 'react-router-dom';
import './SignUp.css';

const SignUp = ()=> {
  return (
    <div className="container">
          <div className="Header">
            <h3>Welcome</h3>
          </div>
          <br></br>
          <div className='Header2'>
          <p><strong>Sign Up to Continue</strong></p>
          </div>
          <input type="text" placeholder="Student Id"  required/>
          <input type="text" placeholder="Username"  required/>
          <input type="text" placeholder="Email"  required/>
          <input type="text" placeholder="Enter Email"  required/>
          <input type="password" placeholder="Password"  required/>
          <button className="button"><strong>Sign Up</strong></button>
          <div className='Footer'>
               <p>Already have an account? <Link to='/login'>Login</Link></p>
          </div>
          </div>
  );
}

export default SignUp;



