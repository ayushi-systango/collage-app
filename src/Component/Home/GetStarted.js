import React from 'react';
import './GetStarted.css';
import history from '../../history';

const GetStarted = (props)=> {
  return (
    <div className="container">
     <img src={require('../../assests/logo/Background.png')}
        width="500"
      alt="network error"></img>
      <div className="Text">
      <h2>Select Your Experience</h2>
      </div>
      <button onClick={()=>history.push('/home')} className="b1">Student</button>

      <button onClick={()=>history.push('/home')} className="b2">Staff/Faculty</button>
      <button onClick={()=>history.push('/home')} className="b3">Alumi</button>
      <button onClick={()=>history.push('/home')} className="b4">Visitor</button>
      <button onClick={()=>history.goBack()}
      className="back">Back</button>
      <button className="next">Next</button>
    </div>
  );
}

export default GetStarted;



