import React,{useState} from 'react';
import history from '../../../../history';
import {CollapsibleComponent,CollapsibleHead,CollapsibleContent} from "react-collapsible-component";
import Plus from '../../../../assests/icons/plus.png';

import './UnderGraduate.css';
 
 const UnderGraduate =(props)=>{
 	const [show, showHandler] = useState(false);

  	const showModal = () => {
    	showHandler(true);
  	}
  
  	const hideModal = () => {
   		showHandler(false);
  	}
    
    const Modal = ({ handleClose, show, children }) => {
  	const showHideClassName = show ? 'modal display-block' : 'modal display-none';

  return (
    <div className={showHideClassName}>
      <section className="applyNowModal">
        {children}
        <button className="cancelButton" onClick={handleClose}>
          Cancel
        </button>
        <button className="submitCancel" onClick={handleClose}>
          Submit
        </button>
      </section>
    </div>
  );
};
        return (
            <div className="MainBodyContent">
            <div className="Navbar">
                <img className="HeaderIcons" 
                src={require('../../../../assests/icons/back.png')} 
                alt="network error"onClick={()=>history.goBack()}></img>
                <h6 className="Heading">Under Graduate</h6>
                <img className="HeaderIcons" src={require('../../../../assests/icons/search.svg')} alt="network error"></img>
                <img className="HeaderIcons" src={require('../../../../assests/icons/notification.png')} alt="network error"></img>
            </div>
            <Modal show={show} handleClose={hideModal} >
                  <div className="ApplyNowHeading">Apply Now</div>
                          <input type="text" placeholder="Full Name"  required/>
                          <input type="text" placeholder="Email Id"  required/>
                          <input type="text" placeholder="Phone Number"  required/>
                          <select className="InputField">
                                <option value="0">Year Of Admission</option>
                                <option value="1">2016</option>
                                <option value="2">2017</option>    
                           </select>
            </Modal>
                <CollapsibleComponent>
                    <CollapsibleHead className="additionalClassForHead">
                    <h5>Bechelor of Commerce (B.Com)</h5>
                    <p>  Lorem ipsum some dummy text</p>
                     <img className="Plus" src={Plus}/>
                    </CollapsibleHead>
                    <CollapsibleContent className="additionalClassForContent"> 
                          <pre className="detail">
                          Degree:BCA<br/>
                          Eligibility:<a>Click here</a><br/>
                          Duration:3 years<br/>
                          CoursesFee:INR 1250000 Annualy<br/>
                           </pre>
                           <button className="ApplyButton" onClick={showModal}>Apply Now</button>
                    </CollapsibleContent>
 
                    <CollapsibleHead className="additionalClassForHead" isExpanded={true}>
                    <h5>Bechelor of Computer Application (BCA)</h5>
                    <p>  Lorem ipsum some dummy text</p>
                     <img className="Plus" src={Plus}/>
                    </CollapsibleHead>

                    <CollapsibleContent className="additionalClassForContent">
                     <pre className="detail">
                          Degree:BCA<br/>
                          Eligibility:<a>Click here</a><br/>
                          Duration:3 years<br/>
                          CoursesFee:INR 1250000 Annualy<br/>
                           </pre>
                           <button className="ApplyButton" onClick={showModal}>Apply Now</button>
                    </CollapsibleContent>
                     <CollapsibleHead className="additionalClassForHead" isExpanded={true}>
                    <h5>Bechelor of Arts (BA)</h5>
                    <p>  Lorem ipsum some dummy text</p>
                     <img className="Plus" src={Plus}/>
                    </CollapsibleHead>
                     <CollapsibleHead className="additionalClassForHead" isExpanded={true}>
                    <h5>Bechelor of Engineering (BE)</h5>
                    <p>  Lorem ipsum some dummy text</p>
                     <img className="Plus" src={Plus}/>
                    </CollapsibleHead>
                     <CollapsibleHead className="additionalClassForHead" isExpanded={true}>
                    <h5>Bechelor of Technology (BTech)</h5>
                    <p>  Lorem ipsum some dummy text</p>
                     <img className="Plus" src={Plus}/>
                    </CollapsibleHead>
                </CollapsibleComponent>
            </div>
        );
}
export default UnderGraduate;