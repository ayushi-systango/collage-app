import React from 'react';
import history from '../../../history';

const Courses =()=>
{
	return(
		<div>
		 <div className="Navbar">
		  <img className="HeaderIcons" 
		  src={require('../../../assests/icons/back.png')} 
		  alt="network error"
		   onClick={()=>history.goBack()}></img>
                <h6 className="Heading">Courses</h6>
                <img className="HeaderIcons" src={require('../../../assests/icons/search.svg')} alt="network error"></img>
                <img className="HeaderIcons" src={require('../../../assests/icons/notification.png')} alt="network error"></img>
            </div>
             <div className="MainContent">
                <img src={require('../../../assests/img/img2.png')} alt="network error"></img>
                <div className="t1">
                    <h3>Ph.D</h3>
                    <p>15 Courses</p>
                </div>
                <img src={require('../../../assests/img/postgraduate.png')} alt="network error"></img>
                <div className="t2">
                    <h3>Post Graduate</h3>
                     <p>15 Courses</p>
                </div>
                <img src={require('../../../assests/img/undergraguate.png')} onClick={()=>history.push('/underGraduate')}
                alt="network error"></img>
                <div className="t3">
                    <h3>Under Graduate</h3>
                     <p>15 Courses</p>
                </div>
                <img src={require('../../../assests/img/diploma.png')} alt="network error"></img>
                <div className="t4">
                    <h3>Diploma</h3>
                     <p>15 Courses</p>
                </div>
            </div>
            <div className="footer">
                <div className="Group">
                    <button>
                        <img src={require('../../../assests/logo/home.png')} alt="university" width="50" />
                    </button  >
                    <button>
                        <img src={require('../../../assests/logo/event.png')} alt="university" width="50" />
                    </button>
                    <button>
                        <img src={require('../../../assests/logo/calander.svg')} alt="university" width="50" />
                    </button>
                    <button>
                        <img src={require('../../../assests/logo/chat.svg')} alt="university"  width="50"/>
                    </button>
                </div>
            </div>
		</div>
		);
}
export default Courses;