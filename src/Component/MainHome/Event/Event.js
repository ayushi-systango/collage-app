import React from 'react';
import './Event.css';
import history from '../../../history';

const Event = (props) => {
	return(
		<div>
		    <div className="Navbar">
                 <img className="HeaderIcons" 
                 src={require('../../../assests/icons/back.png')} 
                 alt="network error"
                  onClick={()=>history.goBack()}></img>
                <h6 className="Heading">Event</h6>
                <img className="HeaderIcons" src={require('../../../assests/icons/search.svg')} alt="network error"></img>
                <img className="HeaderIcons" src={require('../../../assests/icons/notification.png')} alt="network error"></img>
            </div>
            <div className="HeaderButton">
            <button>Academic</button>
            <button>Cultural</button>
            <button>Social</button>
            <button>Sports</button>
            </div>
            <div className="Body">
            <img  
            src={require('../../../assests/img/img1.png')} alt="network error">
            </img>
            <div className="text1">
            <h3>Welcome Week 2020</h3>
            <p>21 Nov- 25 Nov</p>
            </div>
             <img  
            src={require('../../../assests/img/img3.png')} alt="network error">
            </img>
            <div className="text2">
            <h3>Annual Function 2020</h3>
            <p>5 Dec</p>
            </div>
             <img  
            src={require('../../../assests/img/img2.png')} alt="network error">
            </img>
            <div className="text3">
            <h3>Teachers Day 2020</h3>
            <p>5 Sep</p>
            </div>
             <img  
            src={require('../../../assests/img/img4.png')} alt="network error">
            </img>
            </div>
            <div className="footer">
                <div className="Group">
                    <button>
                        <img src={require('../../../assests/logo/home.png')} alt="university" width="50" />
                    </button  >
                    <button>
                        <img src={require('../../../assests/logo/event.png')} alt="university" width="50" />
                    </button>
                    <button>
                        <img src={require('../../../assests/logo/calander.svg')} alt="university" width="50" />
                    </button>
                    <button>
                        <img src={require('../../../assests/logo/chat.svg')} alt="university"  width="50"/>
                    </button>
                </div>
            </div>
		</div>
		);

}

export default Event; 