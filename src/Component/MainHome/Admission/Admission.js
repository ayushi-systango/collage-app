import React from 'react';
import history from '../../../history';
import './Admission.css';

const Admission =()=>
{
	return(
		<div>
		 <div className="Navbar">
		  <img className="HeaderIcons" 
		  src={require('../../../assests/icons/back.png')} 
		  alt="network error"
		   onClick={()=>history.goBack()}></img>
                <h6 className="Heading">Admission</h6>
                <img className="HeaderIcons" src={require('../../../assests/icons/search.svg')} alt="network error"></img>
                <img className="HeaderIcons" src={require('../../../assests/icons/notification.png')} alt="network error"></img>
            </div>
            <div className="Content">
                <img src={require('../../../assests/img/admissionbackground.png')} onClick={()=>history.push('/howToApply')}
                alt="network error"></img>
                    <div className="txt1">
                        <h1>How to Apply</h1>
                        <img src={require('../../../assests/img/c1.png')} alt="network error"></img>
                    </div>
                <img src={require('../../../assests/img/admissionbackground.png')} alt="network error"></img>
                     <div className="txt2">
                        <h1>Important Date</h1>
                        <img src={require('../../../assests/img/c2.png')} alt="network error"></img>
                    </div>
                <img src={require('../../../assests/img/admissionbackground.png')} onClick={()=>history.push('/feeStructure')}
                alt="network error"></img>
                    <div className="txt3">
                        <h1>Fee Structure</h1>
                         <img src={require('../../../assests/img/c3.png')} alt="network error"></img>
                    </div>
                <img src={require('../../../assests/img/admissionbackground.png')} onClick={()=>history.push('/applyNow')}
                alt="network error"></img>
                    <div className="txt4">
                        <h1>Apply Now</h1>
                        <img src={require('../../../assests/img/c4.png')} alt="network error"></img>

                    </div>
            </div>
            <div className="footer">
                <div className="Group">
                    <button>
                        <img src={require('../../../assests/logo/home.png')} alt="university" width="50" />
                    </button  >
                    <button>
                        <img src={require('../../../assests/logo/event.png')} alt="university" width="50" />
                    </button>
                    <button>
                        <img src={require('../../../assests/logo/calander.svg')} alt="university" width="50" />
                    </button>
                    <button>
                        <img src={require('../../../assests/logo/chat.svg')} alt="university"  width="50"/>
                    </button>
                </div>
            </div>
		</div>
		);
}
export default Admission;