import React from 'react';
import history from '../../../../history';
import './ApplyNow.css';

const ApplyNow = ()=> {
  return (
    <div className="MainClass">
      <div className="Navbar">
          <img className="HeaderIcons" src={require('../../../../assests/icons/back.png')} alt="network error"
           onClick={()=>history.goBack()}>
          </img>
          <h6 className="Heading">Apply Now</h6>
          <img className="HeaderIcons" src={require('../../../../assests/icons/search.svg')} alt="network error"></img>
          <img className="HeaderIcons" src={require('../../../../assests/icons/notification.png')} alt="network error"></img>
      </div>
          <input type="text" placeholder="Enter Name"  required/>
          <input type="text" placeholder="Enter Email Address"  required/>
          <input type="text" placeholder="Enter Mobile No."  required/>
          <select className="inputField">
              <option value="0">Select State</option>
              <option value="1">Andhra Pradesh</option>
              <option value="5">Gujarat</option>
              <option value="6">Himachal Pradesh </option>
              <option value="8">Madhya Pradesh</option>
              <option value="9">Maharashtra </option>
              <option value="10"> Punjab</option>
              <option value="11"> Rajasthan </option>
          </select>

          <select className="inputField">
              <option value="0">Select City</option>
              <option value="1">Bhopal</option>
              <option value="2">Dewas</option>
              <option value="3">Guna</option>
              <option value="4">Indore</option>
              <option value="5">Ujjain</option>
          </select>

          <select className="inputField">
              <option value="0">Specialisation</option>
          </select>
          <button className="Button"> Register</button>
    </div>
  );
}

export default ApplyNow;



