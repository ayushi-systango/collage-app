import React from 'react';
import history from '../../../../history';
import './howToApply.css';
const howToApply=()=>{
	return(
		<div>
            <div className="Navbar">
		        <img className="HeaderIcons" src={require('../../../../assests/icons/back.png')} alt="network error"
		         onClick={()=>history.goBack()}>
		         </img>
                <h6 className="Heading">How To Apply</h6>
                <img className="HeaderIcons" src={require('../../../../assests/icons/search.svg')} alt="network error"></img>
                <img className="HeaderIcons" src={require('../../../../assests/icons/notification.png')} alt="network error"></img>
            </div>
            <div className="Head">
               <img src={require('../../../../assests/logo/logo.png')}/>
               <h3> ONLINE ADMISSION PROCESS</h3>
               <h4>(MOAP)-2020</h4>
               <br></br>
            </div>
            <div className="Details">
            <p><strong>Getting started (Online Application)</strong></p>
            <p>On the Lorem Ipsum University Websites (http://www.loermipsumocin/) and click on Admission 2020 tab. Read the instructions about the Admission process carefully.</p>
            <p>Go to the link MOAP-2020. This will be activated only after 20th Dec, 2019.</p>
            <p>The online admission systam will guide you through the complete process.</p>
             
             <p><strong>Phase-1 : Registration</strong></p>
             <p>The first time user should go to New Registration link for signing up with the MOAP-2020, There is no fee for this registration.</p>
             <p>A 'Student Registration Form' will be displayed.</p>
             <p> Once the application has submitted the form, a password will be generated. An email or SMS will notify of the same which the applicant will be able to change.</p>
             <p>After completing the registration process, an Application Number will be generated. This will be visible to you on the pop-up.</p>
            <p>Note down your application number, user id and password carefully. It will be used further while selecting program and choosing course.</p>
            <p>A copy of login details and application number will be notified to the candidates on their registered email id.</p>
            <p>Now, you will be redirected to the login page (Landing Page). Enter your User Name and Password here and press the submit button. </p>
            
              <p><strong>Phase-2 : Program Selection and Course Choice</strong></p>
              <p>Once you are successfully logged into the system, the internal panel displays two basic options.</p>
              <p>The first option provides the facility for selecting desired program and course in the order of priority.</p>
              <p>The applicant will then find a form with various tabs. One will have to fill all the details carefully before selecting desired program and course.</p>
              <p>At the end, a receipt of payment is generated. This contains:</p>
              <p> &nbsp; &nbsp; &nbsp;  &nbsp; A. Details of your filled application form </p>
              <p> &nbsp; &nbsp; &nbsp;  &nbsp; B. Transaction receipt</p>
              <p> &nbsp; &nbsp; &nbsp;  &nbsp; C. Undertaking</p>
              <p> &nbsp; &nbsp; &nbsp;  &nbsp; D. Conditions of selection procedures</p>
              <p> &nbsp; &nbsp; &nbsp;  &nbsp; E. Fees refund and cancellation policies.</p>
            </div>
		</div>
		);
}
export default howToApply;