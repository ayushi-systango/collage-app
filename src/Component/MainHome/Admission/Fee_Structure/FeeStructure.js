import React,{useState} from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';


import history from '../../../../history';
import './FeeStructure.css';
const FeeStructure=(props)=>{
    const [show, showHandler] = useState(false);

    const showModal = () => {
        showHandler(true);
    }
  
    const hideModal = () => {
        showHandler(false);
    }
    
    const Modal = ({ handleClose, show, children }) => {
    const showHideClassName = show ? 'modal display-block' : 'modal display-none';

  return (
    <div className={showHideClassName}>
      <section className="applynowmodal">
        {children}
        <button className="CancelButton" onClick={handleClose}>
          Cancel
        </button>
      </section>
    </div>
  );
};
	return(
		<div className="bodyMain">
            <div className="Navbar">
		        <img className="HeaderIcons" src={require('../../../../assests/icons/back.png')} alt="network error"
		         onClick={()=>history.goBack()}>
		         </img>
                <h6 className="Heading">Fee Structure</h6>
                <img className="HeaderIcons" src={require('../../../../assests/icons/search.svg')} alt="network error"></img>
                <img className="HeaderIcons" src={require('../../../../assests/icons/notification.png')} alt="network error"></img>
            </div>
            <Modal show={show} handleClose={hideModal} >
                <Tabs>
    <TabList className="tablist">
      <Tab >Yearly Fee</Tab>
      <Tab>Semester Fee</Tab>
    </TabList>
 
    <TabPanel>
        <div className="heading">
            Bachelor of Commerce (B.Com)
        </div>
        Duration : <strong>3 Years</strong>
        <div className="TabHeader">
            <p>1st Year &nbsp; &nbsp; &nbsp; &nbsp;  <strong>Rs.19000</strong></p>
            <p>2st Year &nbsp; &nbsp; &nbsp; &nbsp;  <strong>Rs.10190</strong></p>
            <p>3st Year &nbsp; &nbsp; &nbsp; &nbsp;  <strong>Rs.15000</strong></p>
        </div>
    </TabPanel>
    <TabPanel>
        <div className="heading">
            Bachelor of Commerce (B.Com)
        </div>
        Duration : <strong>3 Years</strong>
         <div className="TabHeader">
            <p>1st Sem &nbsp; &nbsp; &nbsp;<strong>Rs.5000</strong> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                   2nd Sem &nbsp; &nbsp; &nbsp; <strong>Rs.5000</strong></p>

            <p>3rd Sem &nbsp; &nbsp; &nbsp; <strong>Rs.5000</strong> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                   4th Sem &nbsp; &nbsp; &nbsp; <strong>Rs.5000</strong></p>
                   
            <p>5th Sem &nbsp; &nbsp; &nbsp; <strong>Rs.5000</strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                  6th Sem &nbsp; &nbsp; &nbsp; <strong>Rs.5000</strong></p>
        </div>
    </TabPanel>
  </Tabs>
            </Modal>
            <div className="mainnn">
                <img src={require('../../../../assests/img/fee.png')} onClick={showModal} alt="network error"></img>
                    <div className="icon1">
                    <h5>Bachelor of Commerce</h5>
                        <img src={require('../../../../assests/icons/Fee.png')}alt="network error"></img>
                    </div>
                <img src={require('../../../../assests/img/fee.png')}alt="network error"></img>
                    <div className="icon2">
                    <h5>Bachelor of Computer Application</h5>
                        <img src={require('../../../../assests/icons/Fee.png')}alt="network error"></img>
                    </div>
                <img src={require('../../../../assests/img/fee.png')}alt="network error"></img>
                    <div className="icon3">
                    <h5>Bachelor of Arts</h5>
                        <img src={require('../../../../assests/icons/Fee.png')}alt="network error"></img>
                    </div>
                <img src={require('../../../../assests/img/fee.png')}alt="network error"></img>
                    <div className="icon4">
                    <h5>Bachelor of Engineering</h5>
                        <img src={require('../../../../assests/icons/Fee.png')}alt="network error"></img>
                    </div>
                <img src={require('../../../../assests/img/fee.png')}alt="network error"></img>
                    <div className="icon5">
                    <h5>Bachelor of Teaching</h5>
                        <img src={require('../../../../assests/icons/Fee.png')}alt="network error"></img>
                    </div>
            </div>
		</div>
		);
}
export default FeeStructure;