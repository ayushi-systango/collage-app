import React from 'react';
import history from '../../../history';

const About =()=>
{
	return(
		<div>
		 <div className="Navbar">
		  <img className="HeaderIcons" 
		  src={require('../../../assests/icons/back.png')} 
		  alt="network error"
		   onClick={()=>history.goBack()}></img>
                <h6 className="Heading">About Us</h6>
                <img className="HeaderIcons" src={require('../../../assests/icons/search.svg')} alt="network error"></img>
                <img className="HeaderIcons" src={require('../../../assests/icons/notification.png')} alt="network error"></img>
            </div>
		</div>
		);
}
export default About;