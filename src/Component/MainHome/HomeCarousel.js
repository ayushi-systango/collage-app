import React from 'react';
import Carousel from 'react-bootstrap/Carousel' 

const HomeCarousel = ()=> {
  return (
   <div>
   <div className='container-fluid' > 
   <Carousel interval={600} keyboard={false} pauseOnHover={true}>  
   <Carousel.Item style={{'height':"400px"}}  >  
   <img style={{'height':"400px"}} className="d-block w-100"src={require('../../assests/img/img1.png')}  />  
   <Carousel.Caption>  
  <h3>Welcome Week 2020</h3>  
  </Carousel.Caption>  
  </Carousel.Item  > 
   <Carousel.Item style={{'height':"400px"}}  >  
   <img style={{'height':"400px"}} className="d-block w-100"src={require('../../assests/img/img2.png')}  />  
   <Carousel.Caption>  
  <h3>Teachers Day  2020</h3>  
  </Carousel.Caption>  
  </Carousel.Item  >  
   <Carousel.Item style={{'height':"400px"}}>  
   <img style={{'height':"400px"}}  className="d-block w-100" src={require('../../assests/img/img3.png')}  />  
    <Carousel.Caption>  
     <h3>Annual Function 2020</h3>  
    </Carousel.Caption>  
   </Carousel.Item>  
 <Carousel.Item style={{'height':"400px"}}>  
  <img style={{'height':"400px"}} className="d-block w-100" src={require('../../assests/img/img4.png')} />  
 <Carousel.Caption>  
 <h3>Halloween 2020</h3>  
</Carousel.Caption>  
</Carousel.Item>  
  </Carousel>  
</div>
</div>

     
  );
}
export default HomeCarousel;
