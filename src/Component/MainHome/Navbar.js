import React from 'react';

import './Navbar.css';

const Navbar = (props) => (
   <div className="Navbar">
    <img  className="HeaderIcons" src={require('../../assests/icons/menu.svg')} alt="network error"></img>
    <h6 className="Heading">Home</h6>
    <img className="HeaderIcons" src={require('../../assests/icons/search.svg')} alt="network error"></img>
     <img className="HeaderIcons" src={require('../../assests/icons/notification.png')} alt="network error"></img>

   </div>
);

export default Navbar; 