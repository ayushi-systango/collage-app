import React from 'react';
import'./MainHome.css';
import HomeCarousel from './HomeCarousel';
import Navbar from './Navbar';
import history from '../../history';

const MainHome=()=> {
 
  return (
  	<div>
    <Navbar/>
    <HomeCarousel/>
  	<div className="row">

        <div className="column">
	    <div className="card">
		 <div className="img-container p-5">
         <img src={require('../../assests/logo/universityCourse.svg')} 
         alt="university" 
         width='100'
         className="card-img-top"
          onClick={()=>history.push('/courses')}
         />
         <p><strong> Courses</strong></p>
         </div>
         </div>
         </div>

         <div className="column">
          <div className="card">
          <div className="img-container p-5">
         <img src={require('../../assests/logo/health.svg')} 
         alt="university" 
         width='100'
         className="card-img-top"
          onClick={()=>history.push('/health')}
         />
         <p><strong> Health</strong></p>
         </div>
         </div>
         </div>

          <div className="column">
          <div className="card">
          <div className="img-container p-5">
         <img src={require('../../assests/logo/about.svg')} 
         alt="university" 
         width='100'
         className="card-img-top"
          onClick={()=>history.push('/about')}
         />
         <p><strong>About</strong></p>
         </div>
         </div>
         </div>

          <div className="column">
          <div className="card">
          <div className="img-container p-5">
         <img src={require('../../assests/logo/admission.svg')} 
         alt="university" 
         width='100'
         className="card-img-top"
          onClick={()=>history.push('/admission')}
         />
         <p><strong>Admission</strong></p>
         </div>
         </div>
         </div>

          <div className="column">
          <div className="card">
          <div className="img-container p-5">
         <img src={require('../../assests/logo/placement.svg')} 
         alt="university" 
         width='100'
         className="card-img-top"
          onClick={()=>history.push('/placement')}
         />
         <p><strong>Placement</strong></p>
         </div>
         </div>
         </div>

          <div className="column">
          <div className="card">
          <div className="img-container p-5">
         <img src={require('../../assests/logo/campus.svg')} 
         alt="university" 
         width='100'
         className="card-img-top"
         onClick={()=>history.push('/campus')}
         />
         <p><strong> Campus</strong></p>
         </div>
         </div>
         </div>

         </div>
         <div className="footer">
         <div className="Group">
         <button>
         <img src={require('../../assests/logo/home.png')} alt="university" width="50" />
         </button  >
         <button onClick={()=>history.push('/event')}>
         <img src={require('../../assests/logo/event.png')} alt="university" width="50" />
         </button>
         <button>
         <img src={require('../../assests/logo/calander.svg')} alt="university" width="50" />
         </button>
         <button>
         <img src={require('../../assests/logo/chat.svg')} alt="university"  width="50"/>
         </button>
         </div>
         </div>
         </div>
    
  );
}

export default MainHome;
