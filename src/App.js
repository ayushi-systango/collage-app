import React from 'react';
import { Router, Switch, Route } from "react-router-dom";

import history from './history';
import Home from './Component/Home/Home';
import GetStarted from './Component/Home/GetStarted';
import SignUp from './Component/Home/SignUp';
import Login from './Component/Home/Login';
import MainHome from './Component/MainHome/MainHome';
import Event from './Component/MainHome/Event/Event';
import Campus from './Component/MainHome/CampusLife/Campus';
import About from './Component/MainHome/AboutUs/About';
import Admission from './Component/MainHome/Admission/Admission';
import howToApply from './Component/MainHome/Admission/How_to_apply/howToApply';
import ApplyNow from './Component/MainHome/Admission/Apply_Now/ApplyNow';
import FeeStructure from './Component/MainHome/Admission/Fee_Structure/FeeStructure';
import Courses from './Component/MainHome/Courses/Courses';
import UnderGraduate from './Component/MainHome/Courses/UnderGraduate/UnderGraduate';
import Health from './Component/MainHome/Health/Health';
import Placement from './Component/MainHome/Placement/Placement';

import './App.css';

const App = ()=> {
  return (
    <div className="App">
     <Router history={history}>
                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/start" component={GetStarted} />
                    <Route exact path="/signup" component={SignUp} />
                    <Route exact path="/home" component={MainHome} />
                    <Route exact path="/event" component={Event} />
                    <Route exact path="/campus" component={Campus} />
                    <Route exact path="/about" component={About} />
                    <Route exact path="/admission" component={Admission} />
                    <Route exact path="/health" component={Health} />
                    <Route exact path="/placement" component={Placement} />
                    <Route exact path="/courses" component={Courses} />
                    <Route exact path="/howToApply" component={howToApply} />
                    <Route exact path="/applyNow" component={ApplyNow} />
                    <Route exact path="/feeStructure" component={FeeStructure} />
                    <Route exact path="/underGraduate" component={UnderGraduate} />

                </Switch>
            </Router>
    </div>
  );
}

export default App;
